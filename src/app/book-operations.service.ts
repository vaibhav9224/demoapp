import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Book } from './book';

@Injectable()
export class BookOperationsService {
  private apiUrl : string = 'https://fakerestapi.azurewebsites.net/api/Books';

  constructor(private http: Http) { }

  getBookList() {
    return this.http.get(this.apiUrl).
    map((res: Response) => res.json())
  }

  addBook(book: Book) {
    return this.http.post(this.apiUrl, book).
    map((res: Response) => res.json())
  }

  getBook(ID: number){
    return this.http.get(this.apiUrl+'/'+ID).
    map((res: Response) => res.json())
  }

  deleteBook(ID: number){
    return this.http.delete(this.apiUrl+'/'+ID).
    map((res: Response) => console.log('deleted'))
  }
}
