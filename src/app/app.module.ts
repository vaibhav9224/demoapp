import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BookListerComponent } from './book-lister/book-lister.component';
import { AppRoutingModule } from './/app-routing.module';
import { HttpModule } from '@angular/http';
import { BookOperationsService } from './book-operations.service';
import { Http } from '@angular/http';
import { AddBookComponent } from './add-book/add-book.component';
import { FormsModule }   from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { OrderByPipePipe } from './order-by-pipe.pipe';


@NgModule({
  declarations: [
    AppComponent,
    BookListerComponent,
    AddBookComponent,
    OrderByPipePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    NgxPaginationModule
  ],
  providers: [BookOperationsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
