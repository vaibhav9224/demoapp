export class Book {

    public ID: number;
    public Title: string;
    public PublishDate: Date;
    public Description: string;

    constructor() {

    }
}
