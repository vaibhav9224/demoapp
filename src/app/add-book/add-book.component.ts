import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Book } from '../book';
import { BookOperationsService } from '../book-operations.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  private book = new Book();
  @Input() currentBookId: number;
  @Input() isEditBook: boolean;
  @Output() valueChange = new EventEmitter();
  private currentBook: Book = new Book();
  bookForm: FormGroup;

  constructor(private bookOperationsService: BookOperationsService) { }

  ngOnInit() {
    if(this.isEditBook) {
        this.editBook(this.currentBookId);
    }
  }

  onSubmit() {
    if(!this.bookForm.form.valid){
      return;
    }
    let newBook: Book = new Book();
    newBook.ID = this.book.ID;
    newBook.Title = this.book.Title;
    newBook.PublishDate = this.book.PublishDate;
    this.bookOperationsService.addBook(this.book).subscribe(data => {
      alert("book added");
      this.valueChange.emit(newBook);
    }) 
  }

  editBook(ID: number){
    this.bookOperationsService.getBook(ID).subscribe(data => {
      this.book.ID = data.ID;
      this.book.Title = data.Title;
      this.book.PublishDate = data.PublishDate;
    })
  }

}
