import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookListerComponent } from './book-lister/book-lister.component';



const routes : Routes = [
  { path: 'book-lister', component: BookListerComponent},
  { path: '', redirectTo: '/book-lister', pathMatch: 'full'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [RouterModule]
})
export class AppRoutingModule { 
  
}
