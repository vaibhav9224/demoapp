import { TestBed, inject } from '@angular/core/testing';

import { BookOperationsService } from './book-operations.service';

describe('BookOperationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookOperationsService]
    });
  });

  it('should be created', inject([BookOperationsService], (service: BookOperationsService) => {
    expect(service).toBeTruthy();
  }));
});
