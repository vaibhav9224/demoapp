import { Component, OnInit } from '@angular/core';
import { BookOperationsService } from '../book-operations.service';
import { Book } from '../book';

@Component({
  selector: 'app-book-lister',
  templateUrl: './book-lister.component.html',
  styleUrls: ['./book-lister.component.css']
})
export class BookListerComponent implements OnInit {
  private bookList: any = [];
  private currentBook: Book = new Book();
  private currentBookId: number;
  private isDesc: boolean = false;
  private column: string = 'Title';
  private direction: number;
  private isEditBook: boolean = false;
  
  constructor(private bookOperationService : BookOperationsService) { }

  ngOnInit() {
    this.loadBookList();
  }

  loadBookList(){
    this.bookOperationService.getBookList().subscribe(data => {
      this.bookList = data;
    })
  }

  addToList(data) {
    this.bookList.push(data);
  }

  addBook() {
    this.currentBookId = null;
  }

  editBook(ID: number){
    this.currentBookId = ID;
  }

  deleteBook(ID: number){
      this.bookOperationService.deleteBook(ID).subscribe(data => {
      let index=this.bookList.map(function(x){ return x.ID; }).indexOf(ID);
      this.bookList.splice(index,1);
    })
  }

  sort(property){
    this.isDesc = !this.isDesc; //change the direction    
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  }
}
