import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookListerComponent } from './book-lister.component';

describe('BookListerComponent', () => {
  let component: BookListerComponent;
  let fixture: ComponentFixture<BookListerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookListerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookListerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
